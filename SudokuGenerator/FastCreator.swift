//
//  FastCreator.swift
//  SudokuGenerator
//
//  Created by Wilhelm Thieme on 6/19/18.
//  Copyright © 2018 Wilhelm Thieme. All rights reserved.
//

import Foundation
import Accelerate

class FastCreator {
    
    var startingNumbers: Int = 25
    
    private var filename = "Seeds.txt"
    private var seedsFound: [Int16: [Int16]]
    
    private func alreadyFound(for diff: Int16) -> Bool {
        return seedsFound[diff] != nil
    }
    
    private var masterSeed: [Int16] = [
        1,2,3,4,5,6,7,8,9,
        4,5,6,7,8,9,1,2,3,
        7,8,9,1,2,3,4,5,6,
        2,3,4,5,6,7,8,9,1,
        5,6,7,8,9,1,2,3,4,
        8,9,1,2,3,4,5,6,7,
        3,4,5,6,7,8,9,1,2,
        6,7,8,9,1,2,3,4,5,
        9,1,2,3,4,5,6,7,8
        ]
    
    var missing: [Int16] {
        var output: [Int16] = []
        for n in 200...799 {
            if !alreadyFound(for: Int16(n)) {
                output.append(Int16(n))
            }
        }
        return output
    }
    
    var max: Int16? {
        return seedsFound.max(by: { $0.0 < $1.0 })?.key
    }
    
    init() {
        seedsFound = FastCreator.readFile(filename)
        
        print("Already \(seedsFound.count) present")
        print("")
        print("Missing:")
        print(missing)
        print("")
        print("Max: \(max ?? 0)")
        FastCreator.printMap(seedsFound[max ?? 0]!)
        print("")
        
        while true {
            var starting = Array<Int16>(repeating: 0, count: 81)
            for _ in 1...startingNumbers {
                if let index = masterSeed.randomIndex {
                    starting[index] = masterSeed[index]
                }
            }
            
            print("Starting:")
            FastCreator.printMap(starting)
            print("")
            
            
            if let seed = findSudoku(with: starting) {
                let diff = MapCreator().grade(seed)
                
                if !alreadyFound(for: diff) {
                    print("Found seed (\(diff)):")
                    FastCreator.printMap(seed)
                    print("\(seedsFound.count) in total")
                    print("")
                    
                    seedsFound[diff] = seed
                    FastCreator.saveFile(filename, seedsFound)
                } else {
                    print("Already have (\(diff)):")
                    FastCreator.printMap(seed)
                    print("")
                }
            }
        }
    }
    
    private func findSudoku(with map: [Int16]) -> [Int16]? {
        var newMap = map
        var sols = solutions(for: newMap)
        
        while sols.count > 1 {
            
            print("\(sols.count) solutions Found")
            print("")
            
            var least: [(Float,Int)] = Array<(Float,Int)>(repeating: (10,10), count: 81)
            
            var transposed: [[Float]] = Array<[Float]>(repeating: [], count: 81)
            
            for n in 0...least.count-1 {                
                for k in 0...sols.count-1 {
                    transposed[n].append(Float(sols[k][n]))
                }
                
                least[n] = leastOccuring(transposed[n])
            }
            
            if let min = least.min(by: {$0.1 < $1.1}), let index = least.index(where: { $0 == min }) {
                if min.0 == 10 {
                    return nil
                }
                newMap[index] = Int16(min.0)
                
                var newSols: [[Int16]] = []
                for solution in sols {
                    if solution[index] == Int16(min.0) {
                        newSols.append(solution)
                    }
                }
                
                sols = newSols
            }
            
            FastCreator.printMap(newMap)
            print("")
            
        }
        return newMap
    }
    
    
    private func solutions(for map: [Int16]) -> [[Int16]] {
        
        var solutions: [[Int16]] = []
        
        solve(map) { (option) in solutions.append(option) }
        
        return solutions
    }
    
    private func solve(_ map: [Int16], solution: (([Int16]) -> Void)?) {
        var possibles: [[Int16]] = Array<[Int16]>(repeating: [1,2,3,4,5,6,7,8,9,10], count: 81)
        
        for n in 0...map.count-1 {
            if map[n] == 0 {
                possibles[n] = possibleNums(in: map, at: n)
            }
        }
        
        if let min = possibles.min(by: { $0.count < $1.count }), let index = possibles.index(of: min) {
            if min.count == 10 {
                solution?(map)
                return
            }
            
            for option in min {
                var newMap = map
                newMap[index] = option
                
                solve(newMap, solution: solution)
            }
        }
    }
    
    private func possibleNums(in map: [Int16], at index: Int) -> [Int16] {
        guard map.count == 81 else {
            return []
        }
        
        guard map[index] == 0 else {
            return [map[index]]
        }
        
        //ROW
        var aRow: [Int16] = [map[index]]
        let r = row(at: index)
        for n in 0...8 {
            let ind = r * 9 + n
            if ind != index {
                aRow.append(map[ind])
            }
        }
        let rowPos = possibleNumsAtIndexZero(aRow)
        
        //COLUMN
        var aCol: [Int16] = [map[index]]
        let c = column(at: index)
        for n in 0...8 {
            let ind = n * 9 + c
            if ind != index {
                aCol.append(map[ind])
            }
        }
        let colPos = possibleNumsAtIndexZero(aCol)
        
        
        //SQUARE
        var aSqr: [Int16] = [map[index]]
        let s = startSquare(at: index)
        for n in 0...2 {
            for k in 0...2 {
                let ind = s + k + n * 9
                if ind != index {
                    aSqr.append(map[ind])
                }
            }
        }
        let sqrPos = possibleNumsAtIndexZero(aSqr)
        
        
        var possibles: [Int16] = []
        for n in 1...9 {
            let n16 = Int16(n)
            if rowPos.contains(n16) && colPos.contains(n16) && sqrPos.contains(n16) {
                possibles.append(n16)
            }
        }
        
        return possibles
    }
    
    private func row(at index: Int) -> Int {
        return index / 9
    }
    
    private func column(at index: Int) -> Int {
        return index % 9
    }
    
    private func startSquare(at index: Int) -> Int {
        let r = row(at: index) / 3
        let c = column(at: index) / 3
        return (r * 27) + (c * 3)
    }
    
    private func possibleNumsAtIndexZero(_ sequence: [Int16]) -> [Int16] {
        guard sequence.count == 9 else {
            return []
        }
        
        guard sequence[0] == 0 else {
            return [sequence[0]]
        }
        
        var possibles: [Int16] = []
        for n in 1...sequence.count {
            let n16 = Int16(n)
            if !sequence.contains(n16) {
                possibles.append(n16)
            }
        }
        return possibles
    }
    
    func leastOccuring(_ arr: [Float]) -> (Float, Int) {
        var counts = [Float: Int]()
        arr.forEach { counts[$0] = (counts[$0] ?? 0) + 1 }
        
        if let (value, count) = counts.min(by: {$0.1 < $1.1}) {
            return (value, count)
        }
        
        return (10, 10)
    }
}

extension FastCreator {
    private static func printMap(_ map: [Int16]) {
        for n in 0...8 {
            let lowerBound = n * 9
            let upperBound = ((n+1) * 9) - 1
            
            print(map[lowerBound...upperBound])
        }
    }
}

extension FastCreator {
    
    private struct SeedData: Codable {
        let difficulty: Int16
        let seed: [Int16]
    }
    
    
    static var homedir: URL {
        return URL(fileURLWithPath: "\(NSHomeDirectory())/Documents/Shared Playground Data")
    }
    
    static func fileExists(_ name: String) -> Bool {
        let filepath = homedir.appendingPathComponent(name).path
        if(FileManager.default.fileExists(atPath: filepath)){
            return true
        }
        return false
    }
    
    static func saveFile(_ name: String, _ data: [Int16:[Int16]]) {
        
        var dataArray: [Data] = []
        for (diff, map) in data {
            if let dat = try? JSONEncoder().encode(SeedData(difficulty: diff, seed: map)) {
                dataArray.append(dat)
            }
        }
        
        let filepath = homedir.appendingPathComponent(name).path
        (dataArray as NSArray).write(toFile: filepath, atomically: true)
    }
    
    static func readFile(_ name: String) -> [Int16:[Int16]] {
        let filepath = homedir.appendingPathComponent(name).path
        if let result = NSArray(contentsOfFile: filepath), let output = result as? [Data] {
            
            var dict: [Int16: [Int16]] = [:]
            
            for data in output {
                if let seed = try? JSONDecoder().decode(SeedData.self, from: data) {
                    dict[seed.difficulty] = seed.seed
                }
            }

            return dict
        }
        return [:]
    }
}
