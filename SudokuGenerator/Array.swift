//
//  Array.swift
//  SudokuGenerator
//
//  Created by Wilhelm Thieme on 6/19/18.
//  Copyright © 2018 Wilhelm Thieme. All rights reserved.
//

import Foundation

extension Array {
    var randomElement: Element? {
        if let rand = self.randomIndex {
            return self[rand]
        } else {
            return nil
        }
    }
    
    var randomIndex: Int? {
        if self.count == 0 {
            return nil
        }
        
        let rand = Int(arc4random_uniform(UInt32(self.count)))
        return rand
    }
}
