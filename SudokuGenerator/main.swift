//
//  main.swift
//  SudokuGenerator
//
//  Created by Wilhelm Thieme on 6/15/18.
//  Copyright © 2018 Wilhelm Thieme. All rights reserved.
//

import Foundation

let seeds = FastCreator.readFile("seeds.txt")

var string: String = ""
for n in 700...799 {
    if let seed = seeds[Int16(n)] {
        print(n)
        string.append("\(String.init(describing: seed)),\n")
    }
}
print(string)


