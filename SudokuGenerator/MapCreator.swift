//
//  MapCreator.swift
//  SudokuGenerator
//
//  Created by Wilhelm Thieme on 6/19/18.
//  Copyright © 2018 Wilhelm Thieme. All rights reserved.
//

import Foundation

class MapCreator  {
    
    static let shared = MapCreator()
    static func configure() {
        let share = shared
        share.startLoading()
    }
    
    struct NewMap {
        let answer: [Int16]
        let starting: [Int16]
        let difficulty: Int16
    }
    
    var newMaps: [NewMap] = []
    
    var baby: [NewMap] = []
    var easy: [NewMap] = []
    var medium: [NewMap] = []
    var hard: [NewMap] = []
    var extreme: [NewMap] = []
    
    init() { }
    
    func startLoading() {
        newMaps = []
        for _ in 1...50 {
            var starting = self.fullSudoku
            for _ in 1...70 {
                let index = self.notEmptyIndexes(in: starting).randomElement!
                starting[index] = 0
            }
            let mapje = self.getStartingNumbers(for: starting)
            
            if mapje.difficulty >= 200 && mapje.difficulty < 300 {
                baby.append(mapje)
                print("Baby: \(mapje.difficulty) \(baby.count)")
            } else if mapje.difficulty >= 300 && mapje.difficulty < 400 {
                easy.append(mapje)
                print("Easy: \(mapje.difficulty) \(easy.count)")
            } else if mapje.difficulty >= 400 && mapje.difficulty < 500 {
                medium.append(mapje)
                print("Medium: \(mapje.difficulty) \(medium.count)")
            } else if mapje.difficulty >= 500 && mapje.difficulty < 600 {
                hard.append(mapje)
                print("Hard: \(mapje.difficulty) \(hard.count)")
                print("\(mapje.starting)")
            } else if mapje.difficulty >= 600 && mapje.difficulty < 700 {
                extreme.append(mapje)
                print("Extreme: \(mapje.difficulty) \(extreme.count)")
                print("\(mapje.starting)")
            } else {
                print("\(mapje.difficulty)")
            }
        }
        printAll()
    }
    
    func printAll() {
        print("")
        
        print("Hard:")
        printOne(hard)
        print("")
        
        print("Extreme:")
        printOne(extreme)
        print("")
        
        
    }
    
    func printOne(_ things: [NewMap]) {
        var string = ""
        for thing in things {
            string = string == "" ? "\(thing.starting)" : "\(string),\n \(thing.starting)"
        }
        print(string)
    }
    
    func grade(_ toSolve: [Int16]) -> Int16 {
        var count = 0
        var sum: Double = 0
        for n in 0...toSolve.count-1 {
            if toSolve[n] == 0 {
                count += 1
            }
            sum += pow(1 - Double(possibleNums(in: toSolve, at: n).count),2)
        }
        
        return Int16(sum) + Int16(count)
    }
    
    private var fullSudoku: [Int16] {
        var starting = Array<Int16>(repeating: 0, count: 81)
        for n in 1...9 {
            if let randomEmpty = emptyIndexes(in: starting).randomElement {
                starting[randomEmpty] = Int16(n)
            }
        }
        return solutions(for: starting, count: 1)[0]
    }
    
    private func getStartingNumbers(for _starting: [Int16]) -> NewMap {
        var starting = _starting
        
        var sols = solutions(for: starting, count: 2)
        while sols.count > 1 {
            
            var counts = Array<Int>(repeating: 0, count: 81)
            for n in 0...starting.count-1 {
                var numbers: [Int16] = []
                for sol in sols {
                    let number = sol[n]
                    if !numbers.contains(number) {
                        numbers.append(number)
                    }
                }
                counts[n] = numbers.count
            }
            
            if let max = counts.max(), let index = counts.index(of: max) {
                starting[index] = sols.randomElement![index]
            }
            
            sols = solutions(for: starting, count: 2)
        }
        
        let difficulty = grade(starting)
        
        return NewMap(answer: sols.first!, starting: starting, difficulty: difficulty)
    }
    
    private func solutions(for map: [Int16], count: Int) -> [[Int16]] {
        
        var solutions: [[Int16]] = []
        //stopSearching = false
        var counter = 0
        
        let _ = solve(map) { (solution) in
            solutions.append(solution)
            counter += 1
            if counter == count {
                return true
            }
            return false
        }
        
        return solutions
    }
    
    private func solve(_ map: [Int16], solution: (([Int16]) -> Bool)) -> Bool {
        var possibleCount: [Int] = Array<Int>(repeating: 10, count: 81)
        
        for n in 0...map.count-1 {
            if map[n] == 0 {
                let count = possibleNums(in: map, at: n).count
                if count > 0 {
                    possibleCount[n] = count
                } else {
                    return false
                }
            }
        }
        
        if let min = possibleCount.min(), let index = possibleCount.index(of: min) {
            if min == 10 {
                return solution(map)
            }
            var possibles = possibleNums(in: map, at: index)
            var newMap: [Int16] = map
            while let i = possibles.randomIndex {
                newMap[index] = possibles[i]
                possibles.remove(at: i)
                let stop = solve(newMap, solution: solution)
                
                if stop {
                    return true
                }
            }
        }
        return false
    }
    
    private func notEmptyIndexes(in map: [Int16]) -> [Int] {
        var output: [Int] = []
        for n in 0...map.count-1 {
            if map[n] != 0 {
                output.append(n)
            }
        }
        return output
    }
    
    private func emptyIndexes(in map: [Int16]) -> [Int] {
        var output: [Int] = []
        for n in 0...map.count-1 {
            if map[n] == 0 {
                output.append(n)
            }
        }
        return output
    }
    
    private func possibleNums(in map: [Int16], at index: Int) -> [Int16] {
        guard map.count == 81 else {
            return []
        }
        
        guard map[index] == 0 else {
            return [map[index]]
        }
        
        //ROW
        var aRow: [Int16] = [map[index]]
        let r = row(at: index)
        for n in 0...8 {
            let ind = r * 9 + n
            if ind != index {
                aRow.append(map[ind])
            }
        }
        let rowPos = possibleNumsAtIndexZero(aRow)
        
        //COLUMN
        var aCol: [Int16] = [map[index]]
        let c = column(at: index)
        for n in 0...8 {
            let ind = n * 9 + c
            if ind != index {
                aCol.append(map[ind])
            }
        }
        let colPos = possibleNumsAtIndexZero(aCol)
        
        
        //SQUARE
        var aSqr: [Int16] = [map[index]]
        let s = startSquare(at: index)
        for n in 0...2 {
            for k in 0...2 {
                let ind = s + k + n * 9
                if ind != index {
                    aSqr.append(map[ind])
                }
            }
        }
        let sqrPos = possibleNumsAtIndexZero(aSqr)
        
        
        var possibles: [Int16] = []
        for n in 1...9 {
            let n16 = Int16(n)
            if rowPos.contains(n16) && colPos.contains(n16) && sqrPos.contains(n16) {
                possibles.append(n16)
            }
        }
        
        return possibles
    }
    
    private func row(at index: Int) -> Int {
        return index / 9
    }
    
    private func column(at index: Int) -> Int {
        return index % 9
    }
    
    private func startSquare(at index: Int) -> Int {
        let r = row(at: index) / 3
        let c = column(at: index) / 3
        return (r * 27) + (c * 3)
    }
    
    private func possibleNumsAtIndexZero(_ sequence: [Int16]) -> [Int16] {
        guard sequence.count == 9 else {
            return []
        }
        
        guard sequence[0] == 0 else {
            return [sequence[0]]
        }
        
        var possibles: [Int16] = []
        for n in 1...sequence.count {
            let n16 = Int16(n)
            if !sequence.contains(n16) {
                possibles.append(n16)
            }
        }
        return possibles
    }
}
